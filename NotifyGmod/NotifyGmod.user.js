// ==UserScript==
// @name           epvp notify gmod
// @namespace      elitepvpers
// @include        /^https?:\/\/www\.elitepvpers\.com\/forum\/complaint-area\/.*$/
// @include        /^https?:\/\/www\.elitepvpers\.com\/forum\/newreply.php.*$/
// @author         Walter Sobchak, modified by Dacx
// @grant          none
// ==/UserScript==

function CreateButton(username, threadlink, mode)
{
	var btn, obj;
	
	if(mode == 'image')
	{
		btn = document.createElement('a');
		btn.href = '//www.elitepvpers.com/forum/newreply.php?do=newreply&noquote=1&t=4323877&gmodnotify=true&username=' + encodeURIComponent(replaceEntities(username)) + '&threadlink=' + threadlink; 
		btn.target = '_blank';
		btn.rel = 'nofollow';
		
		obj = document.createElement('img');
		obj.border = '0';
		obj.src = 'https://i.epvpimg.com/NDfscab.png';
		obj.title = 'Notify a GMod about this thread';
		obj.alt = 'Notify a GMod about this thread';
		obj.setAttribute('style', 'padding-right: 4px');
	}
	else if(mode == 'text')
	{
		obj = document.createElement('a');
		obj.href = '//www.elitepvpers.com/forum/newreply.php?do=newreply&noquote=1&t=4323877&gmodnotify=true&username=' + encodeURIComponent(replaceEntities(username)) + '&threadlink=' + threadlink; 
		obj.target = '_blank';
		obj.rel = 'nofollow';
		
		btn = document.createElement('li');
		btn.id = 'gmod_notify';
		btn.setAttribute('class', 'thead');
		
		obj.appendChild(document.createTextNode('Notify a GMod about this thread'));
	}
	
	btn.appendChild(obj);
	
	return btn;
}

function replaceEntities(username)
{
	return username.replace(/&amp;/g, '&').replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&quot;/g, '"').replace(/&nbsp;/g, "\u00A0");
}

//Main
if(document.location.href.indexOf('/newreply.php?do=newreply&noquote=1&t=') > -1)
{
	var qsParm = new Array();
	function qs() {
		var query = window.location.search.substring(1);
		var parms = query.split('&');
		for (var i=0; i<parms.length; i++) 
		{
			var pos = parms[i].indexOf('=');
			if (pos > 0) 
			{
				var key = parms[i].substring(0,pos);
				var val = parms[i].substring(pos+1);
				qsParm[key] = val;
			}
		}
	}
	qs();
	
	if(qsParm['gmodnotify'] == 'true')
	{
		var textArea = document.getElementById('vB_Editor_001_textarea');
		
		textArea.value = 'User: ' + decodeURIComponent(qsParm['username']) + '\n' +
						 'Thread: [URL]' + qsParm['threadlink'] + '[/URL]\n' +
						 'Assigned GMod: @' + '\n\n' +
            			 'Additional notes: ' + '\n';
	}
}
else if(document.location.href.indexOf('/forum/members/') > -1)
{
	var userBar = document.getElementsByClassName('thead block_row block_title list_no_decoration floatcontainer');
	var usernameBox = document.getElementById('username_box');
	
	if(userBar)
	{
		userBar[0].insertBefore(CreateButton(usernameBox.getElementsByTagName('h1')[0].textContent.trim(), document.location.href, 'text'), document.getElementById('usercss_switch_link'));
	}
}
else
{
	var postsDiv = window.document.getElementById('posts'), bottomBar, username, postTables, usernameTextWrapper, usernameText;

        if(postsDiv)
        {
            postTables = postsDiv.getElementsByTagName('table');
            if(postTables)
            {
                for(var i = 0; i < postTables.length; i++)
                {
                    if(postTables[i].id.indexOf('post') > -1)
                    {
                        username = postTables[i].children[0].rows[1].cells[0].getElementsByClassName('bigusername');
                        bottomBar = postTables[i].children[0].rows[2].cells[1];
                        
                        if((username) && (bottomBar))
                        {
                            if(username[0].children.length > 0)
                            {
                                usernameTextWrapper = username[0].children[0].children[0];
                                usernameText = usernameTextWrapper.children.length ? usernameTextWrapper.children[0].innerHTML : usernameTextWrapper.innerHTML;
                                
                                bottomBar.insertBefore(CreateButton(usernameText, window.location.href, 'image'), bottomBar.children[0]);
                            }
                            else
                            {
                                bottomBar.insertBefore(CreateButton(username[0].innerHTML.replace('<!-- google_ad_section_start(weight=ignore) -->', '').replace('<!-- google_ad_section_end -->', ''), window.location.href, 'image'), bottomBar.children[0]);
                            }
                            
                        }
                    }
                }
            }
        }
}