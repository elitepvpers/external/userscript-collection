// ==UserScript==
// @name           e*pvp Multi Account Check Add-on
// @namespace      elitepvpers
// @include        /^https?:\/\/www\.elitepvpers\.com\/forum\/.*$/
// @author         Muddy Waters
// @description    A script for requesting multi account checks in a few clicks.
// @version        1.3.4
// @grant          none
// ==/UserScript==

// -----------------------------------------------------------------------------
// YOU SHOULD NOT CHANGE ANYTHING BELOW THIS UNLESS YOU KNOW WHAT YOU ARE DOING!
// -----------------------------------------------------------------------------

(function()
{
    main();

    function main()
    {
        var _unsafeWindow = (typeof unsafeWindow != "undefined") ? unsafeWindow : window;

        if(document.location.href.match(/\/forum\/(?:admin|mod)cp\//)) // Don't execute this in iframes...
            return;

        localStorage.setItem('MWMultiAccountCheck_DefaultSettings', JSON.stringify({ enableAjax: true, enableHistory: true, defaultComment: 'Please check the accounts listed below for multi-accounting.', loaderVersion: '1.3.4' }));

        if(!document.getElementById('user_script_utility'))
            includeScript('//userscripts.elitepvpers.com/muddy/resources/classes/USUtility/user_script_utility.js', 'user_script_utility');

        includeScript('//userscripts.elitepvpers.com/muddy/scripts/epvp_multi_account_check/epvp_multi_account_check.js');
    }

    function includeScript(src, id)
    {
        var _script = document.createElement('script');

        if(id)
            _script.setAttribute('id', id);
        _script.setAttribute('type', 'text/javascript');
        _script.setAttribute('src', src);
        document.getElementsByTagName('head')[0].appendChild(_script);
    }
})();