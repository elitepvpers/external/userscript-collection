// ==UserScript==
// @name           e*pvp Quick Move Add-on
// @namespace      elitepvpers
// @include        /^https?:\/\/www\.elitepvpers\.com\/forum\/postings\.php.*$/
// @include        /^https?:\/\/www\.elitepvpers\.com\/forum\/usercp\.php.*$/
// @creator        Muddy Waters
// @description    Adds a few new controls and options when moving topics, speeding up the whole process and making it more comfortable.
// @version        1.2.6
// @grant          none
// ==/UserScript==

// -----------------------------------------------------------------------------
// YOU SHOULD NOT CHANGE ANYTHING BELOW THIS UNLESS YOU KNOW WHAT YOU ARE DOING!
// -----------------------------------------------------------------------------

(function () {
    main();

    function main() {
        var _unsafeWindow = (typeof unsafeWindow != 'undefined') ? unsafeWindow : window;

        if (document.location.href.match(/\/forum\/(?:admin|mod)cp\//) && _unsafeWindow.self != _unsafeWindow.top) // Don't execute this in iframes...
            return;

        localStorage.setItem('MWQuickMove_DefaultSettings', JSON.stringify({
            defaultRedirect: 'none-1-d',
            defaultMessage: '#moved',
            quickForums: [466, 614, 208, 230, 712, 606, 580, 368],
            messageDisabled: false,
            loaderVersion: '1.2.6'
        }));

        if (!document.getElementById('user_script_utility'))
            includeScript('//userscripts.elitepvpers.com/muddy/resources/classes/USUtility/user_script_utility.js', 'user_script_utility');

        includeScript('//userscripts.elitepvpers.com/muddy/scripts/epvp_quick_move/epvp_quick_move.js');
    }

    function includeScript(src, id) {
        var _script = document.createElement('script');

        if (id)
            _script.setAttribute('id', id);
        _script.setAttribute('type', 'text/javascript');
        _script.setAttribute('src', src);
        document.getElementsByTagName('head')[0].appendChild(_script);
    }
})();