# UserScript Collection (Moderation only)

## HowTo Install

All scripts: (you can still decide which scripts you want to add)
Download the userscripts.zip file and import it via tampermonkey (or whatever addon you are using for it). In tampermonkey the import can be found in 'tools'

Specific Script:
Add the user.js-file in the given folder to your browser using Greasemonkey / Tampermonkey.
You can do that by clicking 'Open raw' which can be found on the top right near the Edit Button on the top right of the file.

## Scripts:

### BanRequest

Request a Ban for a user (button visible in their profile and under their posts)

### CustomInfraction

Infract users easily with given templates

### ExtendedModFE

Better Moderator Frontend

### ExtendedPostView

Shows you all posts made by a user in a specific thread

### InstaDelete

One button click away to delete a post you don't like

### MultiAccountCheck

Requests a MultiAccount check for one or multiple, selected users (select them in their profile or posts)

### NotifyGmod

Notify a gmod that a support thread needs attention

There is a 'Peinigen' Version (no Syc) and a one with a simple 'Notify Gmod' Button

### OpenReportSearch

Show the count of open reports in the title bar

### QuickLinks

Quickly paste useful links into a post

### QuickMove

Quickly move a thread with or without a post of yours

### ReasonableDeletion

Quickly delete a post with a given reason (selectable from your own templates)

### MultiScriptLoader

This is an utility script that allows you to include multiple scripts made by LordSill easily.
The most important, which everybody should have is **Show Reports** which shows you all reports of posts in thread view.

### UserScriptControlPanel

This control panel let's you edit settings for different scripts, edit and add infraction templates and much more!

### UserScriptUtility

General Utility for UserScripts

### UserSearch

Search for a user
