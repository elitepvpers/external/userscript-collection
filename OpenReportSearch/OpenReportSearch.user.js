// ==UserScript==
// @name           e*pvp Open Report Search Add-on
// @namespace      elitepvpers
// @include        /^https?:\/\/www\.elitepvpers\.com\/forum\/.*$/
// @include        /^https?:\/\/www\.elitepvpers\.com\/modfe\.php\/.*$/
// @creator        Muddy Waters
// @grant          none
// @version        1.5.1
// ==/UserScript==

(function () {
    function initDefaultSettings() {
        // PLEASE NOTE:
        // The below settings are default settings and merely serve as a backup to restore your settings.
        // Please use the User Script Control Panel for the configuration of this script!

        // Whether to remove the "Upgrade to premium" link from navigation bar.
        Settings.removeLink = true;
        // Whether to dock the navigation bar at the top of your current tab. This idea was entirely stolen from Walter (www.elitepvpers.com/forum/coding-releases/774540-userscript-fixed-userbar.html) in a despicable act of plagiarism!!!
        Settings.dockNavbar = true;

        // If the number of open reports is below this, label will be green.
        Settings.thresholds.low = 10;
        // If the number of open reports is below this, label will be yellow.
        Settings.thresholds.medium = 25;
        // If the number of open reports is below this, label will be orange. If the number of open reports exceeds this value, label will be red.
        Settings.thresholds.high = 50;
    }

    // -----------------------------------------------------------------------------
    // YOU SHOULD NOT CHANGE ANYTHING BELOW THIS UNLESS YOU KNOW WHAT YOU ARE DOING!
    // -----------------------------------------------------------------------------

    var Settings = {
        removeLink: true,
        dockNavbar: true,
        thresholds: {low: 10, medium: 25, high: 50},
        loaderVersion: '1.5.1'
    };

    main();

    function main() {
        var _unsafeWindow = (typeof unsafeWindow != 'undefined') ? unsafeWindow : window;

        if (_unsafeWindow.self != _unsafeWindow.top // Don't execute this in iframes...
            || document.location.href.indexOf('forum/admincp') > -1
            || document.location.href.indexOf('forum/modcp') > -1)
            return;

        initDefaultSettings();
        localStorage.setItem('MWOpenReportSearch_DefaultSettings', JSON.stringify(Settings));

        if (!document.getElementById('user_script_utility'))
            includeScript('//userscripts.elitepvpers.com/muddy/resources/classes/USUtility/user_script_utility.js', 'user_script_utility');

        includeScript('//userscripts.elitepvpers.com/muddy/scripts/epvp_open_report_search/epvp_open_report_search.js');
    }

    function includeScript(src, id) {
        var _script = document.createElement('script');

        if (id)
            _script.setAttribute('id', id);
        _script.setAttribute('type', 'text/javascript');
        _script.setAttribute('src', src);
        document.getElementsByTagName('head')[0].appendChild(_script);
    }
})();