// ==UserScript==
// @name        epvp MultiScriptLoader
// @namespace   elitepvpers
// @description Loads Multiple Userscripts
// @include     https://www.elitepvpers.com/*
// @version     1.1.0
// @author      LordSill
// @grant       none
// ==/UserScript==

(function() {
    var includeJS = function(url, id) {
        var script = $(document.createElement('script'));
        $(script).attr({
            type: 'text/javascript',
            src: url,
            id: id
        });
        $('head').append(script);
    }
    var includeCSS = function(url, id) {
        var stylesheet = $(document.createElement('link'));
        $(stylesheet).attr({
            rel: 'stylesheet',
            type: 'text/css',
            href: url,
            id: id
        });
        $('head').append(stylesheet);
    }
    $.ajax({
        url: '//userscripts.lordsill.de/script/XrIIfXV2xn99N3uwXnW8VY0wO/meta?version=latest',
        dataType: 'jsonp',
        success: function(json) {
            for(var i = 0; i < json.dependencies.length; i++) {
                var url = json.dependencies[i];
                if(url.indexOf('https:') != 0 && url.indexOf('http:') != 0 && url.indexOf('//') != 0) {
                    url = '//userscripts.lordsill.de/dependencies/'+url;
                }
                console.log('['+json.script.name+'] Load Dependency: ' + url);
                var fileType = json.dependencies[i].substr(json.dependencies[i].lastIndexOf('.'));
                var id = json.dependencies[i].substring(json.dependencies[i].lastIndexOf('/')+1, json.dependencies[i].lastIndexOf('.')).replace(/\./g,'-');
                if(fileType == '.js' && !$('script#'+id).length) {
                    includeJS(url, id);
                }
                else if(fileType == '.css' && !$('link#'+id).length) {
                    includeCSS(url, id);
                }
            }
            console.log('['+json.script.name+'] Load Script (Version: '+json.script.version+')');
            includeJS('//userscripts.lordsill.de/script/XrIIfXV2xn99N3uwXnW8VY0wO/load?version=latest');
        }
    });
})();