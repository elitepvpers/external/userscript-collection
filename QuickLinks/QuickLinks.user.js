// ==UserScript==
// @name           e*pvp Quick Links Add-on
// @namespace      elitepvpers
// @include        http://www.elitepvpers.com/forum/*
// @include        https://www.elitepvpers.com/forum/*
// @exclude        http://www.elitepvpers.com/forum/modcp/modschedule.php*
// @exclude        https://www.elitepvpers.com/forum/modcp/modschedule.php*
// @creator        Muddy Waters
// @version        1.1.5
// @grant          none
// @noframes
// ==/UserScript==

(function () {
    // -----------------------------------------------------------------------------
    //          QUICK LINKS DEFAULT SETTINGS
    // -----------------------------------------------------------------------------

    function initSettings() {
        // Choose which BBCode editor iconds you would like the script to add. All options will be added by default, to disable certain options, initialize the respective variables with false.
        // The following editor icons will be added to both quick and advanced reply editors:
        Settings.bb.strike = true;
        Settings.bb.highlight = true;
        Settings.bb.noparse = true;
        Settings.bb.anchor = true;
        Settings.bb.anchorlink = true;
        // The following editor icons will only be added to quick reply editor (just a few for now, might add additional ones later, feel free to let me know if you need some urgently):
        Settings.bb.code = true;
        Settings.bb.html = true;
        Settings.bb.php = true;

        // [When closing or opening a topic via quick reply, you can have the script auto refresh the page after submitting your quick reply. (mode: to be added later)]
        // You can also merely replace the reply buttons so the topic looks closed (or open) without actually refreshing the page. (mode: 1)
        Settings.qrAutoRefreshMode = 1;

        // If this is set to true, the context menu is closed automatically once you selected (i.e. clicked) any quick link or post.
        Settings.autoClose = true;

        // The number of columns to arrange your quick links and posts option in.
        Settings.quickColumns = 2;

        Settings.quickLinks.push(new QuickData('Complaint Area', 'http://www.elitepvpers.com/forum/complaint-area/'));
        Settings.quickLinks.push(new QuickData('elite*gold Support', 'http://www.elitepvpers.com/forum/elite-gold-support/'));
        Settings.quickLinks.push(new QuickData('Scam Info', 'http://www.elitepvpers.com/forum/complaint-area/announcement-gescammed-und-nun.html'));
        Settings.quickLinks.push(new QuickData('General Rules', 'http://www.elitepvpers.com/forum/main/announcement-board-rules-signature-rules.html'));
        Settings.quickLinks.push(new QuickData('BM Rules', 'http://www.elitepvpers.com/forum/trading/announcement-strong-span-style-color-red-black-market-rules-regeln-updated-11-25-2012-span-strong.html'));
        Settings.quickLinks.push(new QuickData('Staff Overview', 'http://www.elitepvpers.com/forum/main/1421118-elitepvpers-staff-overview.html'));
        Settings.quickLinks.push(new QuickData('Penalty Info', 'http://www.elitepvpers.com/forum/main/1329965-de-en-infractions-warnings-things-you-should-know.html'));
        Settings.quickLinks.push(new QuickData('CA Guideline', 'http://www.elitepvpers.com/forum/complaint-area/announcement-span-style-color-red-strong-complaint-area-guideline-read-before-posting-your-complaint-strong-span.html'));
        Settings.quickLinks.push(new QuickData('Legal Email', 'legal@elitepvpers.com'));
        Settings.quickLinks.push(new QuickData('Support Email', 'support@elitepvpers.com'));

        Settings.quickPostOvr = false;
        Settings.quickPosts.push(new QuickData('No Reason', '#closed', true));
        Settings.quickPosts.push(new QuickData('Good Reason', 'I definitely got a good reason for closing this!\n\n#closed', true));
    }

    // -----------------------------------------------------------------------------
    // YOU SHOULD NOT CHANGE ANYTHING BELOW THIS UNLESS YOU KNOW WHAT YOU ARE DOING!
    // -----------------------------------------------------------------------------

    var Settings, InstanceCount;

    InstanceCount = 0;
    Settings =
        {
            quickLinks: [],
            quickPosts: [],
            bb: {},
            loaderVersion: '1.1.5'
        };

    main();

    function main() {
        var _unsafeWindow = (typeof unsafeWindow != 'undefined') ? unsafeWindow : window;

        if (_unsafeWindow.self != _unsafeWindow.top // Don't execute this in iframes...
            || document.location.href.indexOf('//www.elitepvpers.com/forum/modcp/modschedule.php') > -1) // Don't execute on modschedule
            return;

        initSettings();
        localStorage.setItem('MWQuickLinks_DefaultSettings', JSON.stringify(Settings));

        if (!document.getElementById('user_script_utility'))
            includeScript('//userscripts.elitepvpers.com/muddy/resources/classes/USUtility/user_script_utility.js', 'user_script_utility');

        includeScript('//userscripts.elitepvpers.com/muddy/scripts/epvp_quick_links/epvp_quick_links.js');
    }

    function includeScript(src, id) {
        var _script = document.createElement('script');

        if (id)
            _script.setAttribute('id', id);
        _script.setAttribute('type', 'text/javascript');
        _script.setAttribute('src', src);
        document.getElementsByTagName('head')[0].appendChild(_script);
    }

    function QuickData(name, content, isQuickClose) {
        this.name = name;
        this.content = content;
        this.quickClose = (isQuickClose === true);
        this.id = InstanceCount++;
    }
})();