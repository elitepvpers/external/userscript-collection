// ==UserScript==
// @name           e*pvp extended post view
// @namespace      elitepvpers
// @include        /^https?:\/\/www\.elitepvpers\.com\/forum\/.*$/
// @include        /^https?:\/\/www\.elitepvpers\.com\/modfe\.php\/.*$/
// @author         iMostLiked
// @description    Shows you all posts made by a user in a specific thread.
// @version        1.0.0
// @grant          none
// ==/UserScript==

// -----------------------------------------------------------------------------
// YOU SHOULD NOT CHANGE ANYTHING BELOW THIS UNLESS YOU KNOW WHAT YOU ARE DOING!
// -----------------------------------------------------------------------------

(function () {
    main();

    function main() {
        var _unsafeWindow = (typeof unsafeWindow != 'undefined') ? unsafeWindow : window;

        if (document.location.href.match(/\/forum\/(?:admin|mod)cp\//) && _unsafeWindow.self != _unsafeWindow.top)
            return;

        if (!document.getElementById('user_script_utility'))
            includeScript('//userscripts.elitepvpers.com/muddy/resources/classes/USUtility/user_script_utility.js', 'user_script_utility');

        includeScript('//galaxyhacks.net/scripts/extended_post_view.js');
    }

    function includeScript(src, id) {
        var _script = document.createElement('script');

        if (id)
            _script.setAttribute('id', id);
        _script.setAttribute('type', 'text/javascript');
        _script.setAttribute('src', src);
        document.getElementsByTagName('head')[0].appendChild(_script);
    }
})();