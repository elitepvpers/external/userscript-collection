// ==UserScript==
// @name           e*pvp Extended ModFE New
// @namespace      elitepvpers.com
// @include        //www.elitepvpers.com/modfe.php*
// @include        https://www.elitepvpers.com/modfe.php*
// @author         .aNNdii#, MrSm!th
// @description    A better ModFE
// @version        2.0.1
// @grant          none
// ==/UserScript==

// -----------------------------------------------------------------------------
// RELEASE VERSION - DO NOT CHANGE ANYTHING BELOW UNLESS YOU KNOW WHAT YOU ARE DOING
// -----------------------------------------------------------------------------

(function () {
    var DEBUG = false;
    var SCRIPT_NAME = 'ModFE';
    var SCRIPT_BASE = '//userscripts.elitepvpers.com/muddy/smdev/modfe/';

    var USLOADER_SCRIPT_NAME = 'USLoader';
    var USLOADER_BASE = '//userscripts.elitepvpers.com/muddy/smdev/usloader/';
    var USLOADER_ID = 'epvp_smdev_scriptloader';

    main();

    function main() {
        if (!document.getElementById(USLOADER_ID)) {
            includeScript(getScriptUrl(USLOADER_SCRIPT_NAME, USLOADER_BASE), USLOADER_ID);
        }

        includeRootModule();
    }

    function includeRootModule() {
        var baseUrl = getBranchBaseUrl(SCRIPT_BASE);
        var scriptUrl = getScriptUrl(SCRIPT_NAME, SCRIPT_BASE);

        var rootModule = {
            name: SCRIPT_NAME,
            baseUrl: baseUrl,
            scriptUrl: scriptUrl,
            useMinified: !DEBUG
        };

        injectRootModuleDefinition(rootModule);
        includeScript(scriptUrl);
    }

    function injectRootModuleDefinition(rootModule) {
        var funcTemplate = function() {
            usLoader = typeof usLoader === 'undefined' ? {} : usLoader;
            usLoader.rootModules = typeof usLoader.rootModules === 'undefined' ? [] : usLoader.rootModules;

            usLoader.rootModules['$$scriptName$$'] = $$rootModule$$;
        };

        var functionCode = funcTemplate.toString().replace('$$scriptName$$', SCRIPT_NAME)
            .replace('$$rootModule$$', JSON.stringify(rootModule));

        injectScript('(' + functionCode + ')();');
    }

    function getScriptUrl(scriptBaseName, baseUrl) {
        return getBranchBaseUrl(baseUrl) + getScriptName(scriptBaseName);
    }

    function getScriptName(baseName) {
        return baseName + (DEBUG ? '.js' : '.min.js');
    }

    function getBranchBaseUrl(baseUrl) {
        return baseUrl + (DEBUG ? 'dev/' : 'release/');
    }

    function includeScript(src, id) {
        var scriptTag = document.createElement('script');
        if (id) {
            scriptTag.setAttribute('id', id);
        }
        scriptTag.setAttribute('type', 'text/javascript');
        scriptTag.setAttribute('src', src);
        document.getElementsByTagName('head')[0].appendChild(scriptTag);
    }

    function injectScript(code, id) {
        var scriptTag = document.createElement('script');
        if (id) {
            scriptTag.setAttribute('id', id);
        }
        scriptTag.setAttribute('type', 'text/javascript');
        scriptTag.innerHTML = '' + code;
        document.getElementsByTagName('head')[0].appendChild(scriptTag);
    }

})();