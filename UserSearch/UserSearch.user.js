// ==UserScript==
// @name           epvp User Search Add-on
// @namespace      elitepvpers.com
// @include        *//www.elitepvpers.com/forum/
// @exclude        //www.elitepvpers.com/forum/modcp/modschedule.php*
// @exclude        //www.elitepvpers.com/forum/stafflist.php*
// @author         Muddy Waters
// @description    A script for searching user profiles by username.
// @version        1.0.6
// @grant          none
// ==/UserScript==

// -----------------------------------------------------------------------------
// YOU SHOULD NOT CHANGE ANYTHING BELOW THIS UNLESS YOU KNOW WHAT YOU ARE DOING!
// -----------------------------------------------------------------------------

(function () {
    main();

    function main() {
        var _unsafeWindow = (typeof unsafeWindow != 'undefined') ? unsafeWindow : window;

        if (_unsafeWindow.self != _unsafeWindow.top // Don't execute this in iframes...
            || document.location.href.indexOf('//www.elitepvpers.com/forum/modcp/modschedule.php') > -1) // Don't execute on modschedule (Non-GM)
            return;

        localStorage.setItem("MWUserSearch_DefaultSettings", JSON.stringify({
            resultsThresh: 5,
            showModForums: true,
            interactiveTooltips: true,
            loaderVersion: '1.0.6'
        }));

        if (!document.getElementById('user_script_utility'))
            includeScript('//userscripts.elitepvpers.com/muddy/resources/classes/USUtility/user_script_utility.js', 'user_script_utility');

        includeScript('//userscripts.elitepvpers.com/muddy/scripts/epvp_user_search/epvp_user_search.js');
    }

    function includeScript(src, id) {
        var _script = document.createElement('script');

        if (id)
            _script.setAttribute('id', id);
        _script.setAttribute('type', 'text/javascript');
        _script.setAttribute('src', src);
        document.getElementsByTagName('head')[0].appendChild(_script);
    }
})();