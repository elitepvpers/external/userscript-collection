// ==UserScript==
// @name        e*pvp User Script Control Panel
// @namespace   elitepvpers
// @include     /^https?:\/\/www\.elitepvpers\.com\/forum\/.*$/
// @version     1.0.5
// @grant       none
// ==/UserScript==

// -----------------------------------------------------------------------------
// YOU SHOULD NOT CHANGE ANYTHING BELOW THIS UNLESS YOU KNOW WHAT YOU ARE DOING!
// -----------------------------------------------------------------------------

(function () {
    main();

    function main() {
        var _unsafeWindow = (typeof unsafeWindow != 'undefined') ? unsafeWindow : window;

        if (_unsafeWindow.self != _unsafeWindow.top) // Don't execute this in iframes...
            return;

        localStorage.setItem('MWControlPanel_DefaultSettings', JSON.stringify({loaderVersion: '1.0.5'}));

        if (!document.getElementById('user_script_utility'))
            includeScript('//userscripts.elitepvpers.com/muddy/resources/classes/USUtility/user_script_utility.js', 'user_script_utility');

        includeScript('//userscripts.elitepvpers.com/muddy/scripts/epvp_user_script_cp/epvp_user_script_control_panel.js');
    }

    function includeScript(src, id) {
        var _script = document.createElement('script');

        if (id)
            _script.setAttribute('id', id);
        _script.setAttribute('type', 'text/javascript');
        _script.setAttribute('src', src);
        document.getElementsByTagName('head')[0].appendChild(_script);
    }
})();