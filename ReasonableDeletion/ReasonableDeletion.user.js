// ==UserScript==
// @name           e*pvp Reasonable Deletion Add-on
// @namespace      elitepvpers
// @include        *//www.elitepvpers.com/forum/*
// @creator        Muddy Waters
// @version        1.3.2
// @grant          none
// @noframes
// ==/UserScript==

(function () {
    function initSettings() {
        // Whether to always close deletion form after you delete posts/threads.
        Settings.autoClose = false;

        // Whether to use a combo box for selecting deletion reasons instead of radio buttons.
        Settings.altSelection = false;

        // Whether to automatically submit post deletion form as you select a reason.
        Settings.autoSubmit = false;

        // This can be used to specify how the radio boxes for selection deletion reasons are arranged.
        Settings.radioBtnsPerRow = 4;

        // Choose on what pages the deletion form is shown:
        Settings.showDeletionForm.threadView = true;
        Settings.showDeletionForm.forumView = false;
        Settings.showDeletionForm.searchPosts = true;
        Settings.showDeletionForm.searchThreads = true;

        // Define reasons for post deletion:
        Settings.deletionReasons.post.push('Spam');
        Settings.deletionReasons.post.push('Advertising');
        Settings.deletionReasons.post.push('Malware');
        Settings.deletionReasons.post.push('Insulted other member(s)');
        Settings.deletionReasons.post.push('Inappropriate language');
        Settings.deletionReasons.post.push('Double post');
        Settings.deletionReasons.post.push('Free push');
        Settings.deletionReasons.post.push('Early bump');
        Settings.deletionReasons.post.push('Unrequested MM offer');
        Settings.deletionReasons.post.push('Unauthorized offer');
        Settings.deletionReasons.post.push('Reporting scammers publicly is not allowed, please use the TBM system or the complaint area [TBM#1.1]');
        Settings.deletionReasons.post.push('Due to the release of the Bump Button bumping with posts is no longer allowed [TBM#4.1]');
        Settings.deletionReasons.post.push('Criticising the price or other things that may negatively influence the trade are absolutely forbidden [TBM#4.2]');
        Settings.deletionReasons.post.push('Please only post if you are interested in a trade [TBM#4.2]');
        Settings.deletionReasons.post.push('You are not allowed to offer your vouch service if not explicitly asked [TBM#4.3]');

        // Define reasons for thread deletion:
        Settings.deletionReasons.thread.push('Spam');
        Settings.deletionReasons.thread.push('Advertising');
        Settings.deletionReasons.thread.push('Malware');
        Settings.deletionReasons.thread.push('Multi thread');
        Settings.deletionReasons.thread.push('Scammer accusation');
        Settings.deletionReasons.thread.push('Phishing');
    }

    // -----------------------------------------------------------------------------
    // YOU SHOULD NOT CHANGE ANYTHING BELOW THIS UNLESS YOU KNOW WHAT YOU ARE DOING!
    // -----------------------------------------------------------------------------

    var Settings = {
        deletionReasons: {thread: [], post: []},
        showDeletionForm: {},
        altSelection: false,
        autoSubmit: false,
        autoClose: false,
        radioBtnsPerRow: 4,
        highlightSelection: false,
        syncSelection: false,
        loaderVersion: '1.3.2'
    };

    main();

    function main() {
        var _unsafeWindow = (typeof unsafeWindow != 'undefined') ? unsafeWindow : window;

        if (_unsafeWindow.self != _unsafeWindow.top) // Don't execute this in iframes...
            return;

        initSettings();
        localStorage.setItem('MWReasonableDeletion_DefaultSettings', JSON.stringify(Settings));

        if (!document.getElementById('user_script_utility'))
            includeScript('//userscripts.elitepvpers.com/muddy/resources/classes/USUtility/user_script_utility.js', 'user_script_utility');

        includeScript('//userscripts.elitepvpers.com/muddy/scripts/epvp_reasonable_deletion/epvp_reasonable_deletion.js');
    }

    function includeScript(src, id) {
        var _script = document.createElement('script');

        if (id)
            _script.setAttribute('id', id);
        _script.setAttribute('type', 'text/javascript');
        _script.setAttribute('src', src);
        document.getElementsByTagName('head')[0].appendChild(_script);
    }
})();